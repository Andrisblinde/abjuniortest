-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2018 at 09:41 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `size` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(12) DEFAULT NULL,
  `length` int(13) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type`, `size`, `weight`, `height`, `width`, `length`) VALUES
(6, 'KL589541s', 'Crime and Punishment', 20, 'Book', NULL, 2, NULL, NULL, NULL),
(16, 'HJ4566789', 'The Godfather', 10, 'DVD', 2000, NULL, NULL, NULL, NULL),
(10, 'CA9123457', 'Captain America', 10, 'DVD', 1800, NULL, NULL, NULL, NULL),
(11, 'BCS505842', 'Cocobolo desk', 1000, 'Furniture', NULL, NULL, 60, 80, 180),
(12, 'HP56489712', 'Harry Potter', 10, 'Book', NULL, 2, NULL, NULL, NULL),
(15, 'KL45678664', 'District 9', 10, 'DVD', 1800, NULL, NULL, NULL, NULL),
(56, 'SK11111111111', 'Some DVD', 5, 'DVD', 700, NULL, NULL, NULL, NULL),
(57, 'SK2222222222', 'Some Book', 15, 'Book', NULL, 2, NULL, NULL, NULL),
(59, 'SK333333333333', 'Another DVD', 8, 'DVD', 700, NULL, NULL, NULL, NULL),
(69, 'SK5555555', 'Some Furniture', 80, 'Furniture', NULL, NULL, 50, 60, 120),
(68, 'SK444444444', 'Yet another DVD', 6, 'DVD', 700, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
