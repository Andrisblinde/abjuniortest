<?PHP header("Content-Type: text/html; charset=utf-8");

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 'test';

$conn = mysqli_connect($dbhost,  $dbuser, $dbpass, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Product List</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<meta name="description" content="this is a test prpoduct list page" />
<meta name="keywords" content="test,product,list" />
<link rel="stylesheet" href="style.css" />
</head>

<body>
<div class="page_container">
<header>
    <h1>Product list</h1>

    <div id="add_new_product">
	<div class="button"><a href="new_product.html"><svg><rect x="0" y="0" fill="none" width="100%" height="100%"/></svg>Add new product</a></div>
	</div>
</header>

<div id="products">

<form action="<?php $_PHP_SELF ?>" method="post">

<?php
   
$sql = "SELECT * FROM products ORDER by id ASC";

$result = mysqli_query($conn, $sql);

while ($product=$result->fetch_assoc()): ?>
	<div class="item">
		<input type="checkbox" name="checkbox[]" value="<?=$product['id']?>" />
		<h2 class="sku"><?= $product['sku'] ?></h2>
    	<h2 class="name"><?= $product['name'] ?></h2>
    	<h2 class="price"><?= $product['price'] ?>$</h2>
    	<h2 class="type"><?= $product['type'] ?></h2>
    	<h2 class="size"><?php
        if ($product['type']==="DVD") {
            printf($product['size']);
        echo "MB";
      } else if ($product['type']==="Book") {
        printf($product['weight']);
        echo "kg";
      } else {
        printf($product['height']);
        echo "cm x";
        printf($product['width']);
        echo "cm x";
        printf($product['length']);
        echo "cm";
        }
        ?>
        <!--</h2>
    	<h2 class="weight"><?= $product['weight'] ?></h2>
    	<h2 class="height"><?= $product['height'] ?></h2>
    	<h2 class="width"><?= $product['width'] ?></h2>
    	<h2 class="length"><?= $product['length'] ?></h2>-->
	</div>
<?php endwhile; ?>

<div id="bulk_delete">
<button type="submit" name="delete" value="delete"><svg><rect x="0" y="0" fill="none" width="100%" height="100%"/></svg>Delete selected</a></button> 
</form>

<?php
if(isset($_POST['delete'])) {
 $count=array();
 $count=count($_POST['checkbox']);
 for($i=0;$i<$count;$i++)
  {
     $delete=$_POST['checkbox'][$i];
     $query="DELETE FROM products WHERE id=".$delete;
     $result = mysqli_query($conn, $query);
     echo("<meta http-equiv='refresh' content='1'>");
  }
}

?>
</div>       

</div>
</div>
</body>
</html>